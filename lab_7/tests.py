from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .views import (
    index, add_friend, validate_npm, delete_friend, friend_list,
    get_friend_list, friend_description, paginate_page
)
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

# Create your tests here.
class lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_get_friend_list_data_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_friend_description_url_is_exist(self):
        friend = Friend.objects.create(friend_name="Pina Korata", npm="1606123456")
        response = Client().post('/lab-7/friend-list/description/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 200)

    def test_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])

    def test_add_friend(self):
        response_post = Client().post(
            '/lab-7/add-friend/',
            {'name':"irfani", 'npm':"1606", 'alamat':" ", 'birth':" ", 'study':" "}
        )
        self.assertEqual(response_post.status_code, 200)

    def test_invalid_sso_raise_exception(self):
        username = "irfani"
        password = "irfani"
        csui_helper = CSUIhelper()
        with self.assertRaises(Exception) as context:
            csui_helper.instance.get_access_token(username, password)
        self.assertIn("irfani", str(context.exception))

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken':False})

    def test_delete_friend(self):
        friend = Friend.objects.create(friend_name="Pina Korata", npm="1606123456")
        response = Client().post('/lab-7/friend-list/delete-friend/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, Friend.objects.all())

    def test_empty_page(self):
        response = paginate_page(2, Friend.objects.all())
        self.assertEqual(response['page_range'][0], 1)
    
    def test_page_not_integer(self):
        response = Client().get('/lab-7/?page=dianti')
        # self.assertEqual(response.status_code,200)
        # response = Friend.objects.create(friend_name= 'dianti', npm= '16066')
        # response = paginate_page(1.5, Friend.objects.all())
        # self.assertEqual(response['page_range'][0], 1)
        # # self.assertEqual(0, response.end_index())

